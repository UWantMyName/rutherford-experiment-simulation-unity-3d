﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDivertedParticles4 : MonoBehaviour
{

    public GameObject particlePrefab;
    public int nrParticles = 0;
    private int TimeLapse = 0;

    public bool Paused = false;

    void Update()
    {
        if (!Paused)
        {
            float y, z;

            y = Random.Range(4f, 6f);
            z = Random.Range(2f, 6f); z *= -1;

            TimeLapse++;
            if (TimeLapse > 10)
            {
                TimeLapse = 0;
                Instantiate(particlePrefab, new Vector3(-12.4f, y, z), Quaternion.identity);
                nrParticles++;
            }
        }
    }
}

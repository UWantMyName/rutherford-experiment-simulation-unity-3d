﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner2 : MonoBehaviour
{
    public GameObject particlePrefab;
    public int nrParticles = 0;
    private int TimeLapse = 0;
    public float forwardForce;

    private void Update()
    {
        float y, z;

        y = Random.Range(4f, 6f);
        z = Random.Range(2f, 6f); z *= -1;

        TimeLapse++;
        if (TimeLapse > 10)
        {
            TimeLapse = 0;
            Instantiate(particlePrefab, new Vector3(-7.64f, y, z), Quaternion.identity);
            nrParticles++;
        }

        /*
        if (nrParticles < 30)
        {
            Instantiate(particlePrefab, new Vector3(-48.91f, y, z), Quaternion.identity);
            nrParticles++;
        }
        */
        /*
        float movement = Input.GetAxis("Horizontal");

        Instantiate(particlePrefab, new Vector3(-48.91f, y, z), Quaternion.identity);
        float dirX = movement * forwardForce * Time.deltaTime;
        Vector3 finalTranform = new Vector3(dirX, 0, 0);
        transform.Translate(finalTranform);
        */
    }
}

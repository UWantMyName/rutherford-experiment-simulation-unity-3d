﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        string Name = collision.gameObject.name;

        if (Name == "AlfaParticle") Destroy(collision.gameObject);
    }

}

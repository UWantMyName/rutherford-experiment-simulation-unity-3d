﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCollider : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "AlfaParticlePrefab" || collision.gameObject.name == "AlfaParticlePrefab(Clone)") Destroy(collision.gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle5 : MonoBehaviour {

    public Rigidbody rb;
    public float forwardForce;
    public float sideForce;

    private bool randomize = false;
    private int x;

    void SetSideForce()
    {
        if (!randomize)
        {
            sideForce = Random.Range(0.0f, 40.0f);
            randomize = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        SetSideForce();
        rb.AddForce(-forwardForce * Time.deltaTime, 0, -sideForce * Time.deltaTime, ForceMode.VelocityChange);
    }

    private void OnCollisionEnter(Collision collision)
    {
        string Name = collision.gameObject.name;

        if (Name == "GoldSheetPrefab")
        {
            Destroy(gameObject);
        }
        else if (Name == "FPSController" || Name == "AlfaParticlePrefab" || Name == "AlfaParticlePrefab(Clone)") Physics.IgnoreCollision(collision.collider, rb.GetComponent<Collider>());
        else if (Name == "Wall0" || Name == "Wall1" || Name == "Wall2" || Name == "Wall3") Destroy(gameObject);
        else if (Name == "Screen" || Name == "SmallScreen" || Name == "Spawner" || Name == "GeneratorPartAlfa") Destroy(gameObject);
    }
}

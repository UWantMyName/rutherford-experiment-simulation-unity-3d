﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetCollider : MonoBehaviour
{
    public Rigidbody rb;
    public bool CheckCollide = false;
    public GameObject FirstSpawner;
    public GameObject SecondSpawner;
    public GameObject ThirdSpawner;
    public GameObject FourthSpawner;
    public GameObject FifthSpawner;
    public GameObject SixthSpawner;

    private void OnCollisionEnter(Collision collision)
    {
        string Name = collision.gameObject.name;

        if (Name == "AlfaParticlePrefab" || Name == "AlfaParticlePrefab(Clone)")
        {
            //Physics.IgnoreCollision(collision.collider, rb.GetComponent<Collider>());
            Destroy(collision.gameObject.GetComponent<Rigidbody>().gameObject);
            
            if (CheckCollide == false)
            {
                CheckCollide = true;
                //Debug.Log("Spawner");
                Instantiate(FirstSpawner);
                Instantiate(SecondSpawner);
                Instantiate(ThirdSpawner);
                Instantiate(FourthSpawner);
                Instantiate(FifthSpawner);
                Instantiate(SixthSpawner);
            }
        }
    }

}

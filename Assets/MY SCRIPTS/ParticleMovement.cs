﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleMovement : MonoBehaviour
{
    public Rigidbody rb;

    private void OnCollisionEnter(Collision collision)
    {
        string Name = collision.gameObject.name;

        if (Name == "GoldSheet")
        {
            Physics.IgnoreCollision(collision.collider, rb.GetComponent<Collider>());
        }
        else if (Name == "FPSController" || Name == "AlfaParticlePrefab" || Name == "AlfaParticlePrefab(Clone)" || Name == "Collider") Physics.IgnoreCollision(collision.collider, rb.GetComponent<Collider>());
        else if (Name == "Wall0" || Name == "Wall1" || Name == "Wall2" || Name == "Wall3" || Name == "Screen") Destroy(rb.gameObject);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused) Resume();
            else Pause();
        }
    }

    public void Resume()
    {
        if (GameObject.Find("Spawner6(Clone)") != null) pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        //start the generators
        GameObject.Find("Spawner").GetComponent<ParticleSpawner>().Paused = false;
        if (GameObject.Find("Spawner6(Clone)") != null)
        {
            GameObject.Find("Spawner1(Clone)").GetComponent<SpawnDivertedParticles>().Paused = false;
            GameObject.Find("Spawner2(Clone)").GetComponent<SpawnDivertedParticles2>().Paused = false;
            GameObject.Find("Spawner3(Clone)").GetComponent<SpawnDivertedParticles3>().Paused = false;
            GameObject.Find("Spawner4(Clone)").GetComponent<SpawnDivertedParticles4>().Paused = false;
            GameObject.Find("Spawner5(Clone)").GetComponent<SpawnDivertedParticles5>().Paused = false;
            GameObject.Find("Spawner6(Clone)").GetComponent<SpawnDivertedParticles6>().Paused = false;
        }
        GameIsPaused = false;
    }

    public void Pause()
    {
        if (GameObject.Find("Spawner6(Clone)") != null) pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        //stop the generators
        GameObject.Find("Spawner").GetComponent<ParticleSpawner>().Paused = true;
        if (GameObject.Find("Spawner6(Clone)") != null)
        {
            GameObject.Find("Spawner1(Clone)").GetComponent<SpawnDivertedParticles>().Paused = true;
            GameObject.Find("Spawner2(Clone)").GetComponent<SpawnDivertedParticles2>().Paused = true;
            GameObject.Find("Spawner3(Clone)").GetComponent<SpawnDivertedParticles3>().Paused = true;
            GameObject.Find("Spawner4(Clone)").GetComponent<SpawnDivertedParticles4>().Paused = true;
            GameObject.Find("Spawner5(Clone)").GetComponent<SpawnDivertedParticles5>().Paused = true;
            GameObject.Find("Spawner6(Clone)").GetComponent<SpawnDivertedParticles6>().Paused = true;
        }
        GameIsPaused = true;
    }

    public void QuitButton_Click()
    {
        /*
        Time.timeScale = 1f;
        pauseMenuUI.SetActive(false);
        GameIsPaused = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        */
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    public GameObject particlePrefab;
    public int nrParticles = 0;
    private int TimeLapse = 0;

    public bool Paused = false;

    private void Update()
    {
        if (!Paused)
        {
            float y, z;

            y = Random.Range(4f, 6f);
            z = Random.Range(2f, 6f); z *= -1;

            Instantiate(particlePrefab, new Vector3(-48.91f, y, z), Quaternion.identity);
            nrParticles++;
        }
    }
}
